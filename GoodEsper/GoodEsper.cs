﻿//!CompilerOption:AddRef:clipper_library.dll

using System.Collections.Generic;
using System.Threading.Tasks;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using GoodEsper.Behaviours;
using GoodEsper.Controllers;
using GoodEsper.Dodge;

namespace GoodEsper
{
    /// <summary>
    ///     GoodEsper - A simple Esper routine designed to work with level 50 Espers.
    /// </summary>
    public class GoodEsper : CombatRoutineBase
    {
        private readonly BehaviourRunner _behaviourRunner;
        private readonly BehaviourRunner _combatBehaviourRunner;
        private readonly SpellController _spellController;

        public GoodEsper()
        {
            _spellController = new SpellController();
            _combatBehaviourRunner = new BehaviourRunner(new List<Behaviour>
            {
                new Survival(_spellController),
                new Combat(_spellController)
            });

            _behaviourRunner = new BehaviourRunner(new List<Behaviour>
            {
                new Rest(_spellController)
            });

            Logger.Log("Initialised");
        }

        public override string Name
        {
            get { return Info.Name; }
        }

        public override string Author
        {
            get { return Info.Author; }
        }

        public override string Version
        {
            get { return Info.Version; }
        }

        public override async Task Combat()
        {
            if (GameManager.LocalPlayer.IsDead)
            {
                Evading.IsEvading = false;
                return;
            }

            await _combatBehaviourRunner.Execute();
        }

        public override void Pulse()
        {
            if (Evading.IsEvading && !GameManager.LocalPlayer.Position.IsInDanger() && GameManager.LocalPlayer.IsMoving)
            {
                Evading.IsEvading = false;
                Navigator.Stop();
                Logger.Log("Finished evading");
            }
            _behaviourRunner.Execute();
            base.Pulse();
        }
    }
}