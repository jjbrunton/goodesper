﻿using Buddy.Common.Math;

namespace GoodEsper.Dodge.Math
{
    public class Circle
    {
        private const int CircleLineSegmentN = 22;
        public Vector3 Center;
        public float Radius;

        public Circle(Vector3 center, float radius)
        {
            Center = center;
            Radius = radius;
        }

        public Polygon ToPolygon(int offset = 0, float overrideWidth = -1)
        {
            var result = new Polygon();
            float outRadius = (overrideWidth > 0
                ? overrideWidth
                : (offset + Radius)/(float) System.Math.Cos(2*System.Math.PI/CircleLineSegmentN));

            for (int i = 1; i <= CircleLineSegmentN; i++)
            {
                double angle = i*2*System.Math.PI/CircleLineSegmentN;
                var point = new Vector3(Center.X + outRadius*(float) System.Math.Cos(angle),
                    Center.Y + outRadius*(float) System.Math.Sin(angle), Center.Z);
                result.Add(point);
            }

            return result;
        }
    }
}