﻿using System.Collections.Generic;
using Buddy.Common.Math;
using ClipperLib;

namespace GoodEsper.Dodge.Math
{
    public class Polygon
    {
        public List<Vector3> Points = new List<Vector3>();

        public void Add(Vector3 point)
        {
            Points.Add(point);
        }

        public List<IntPoint> ToClipperPath()
        {
            var result = new List<IntPoint>(Points.Count);

            foreach (Vector3 point in Points)
            {
                result.Add(new IntPoint(point.X, point.Y));
            }

            return result;
        }

        public bool IsOutside(Vector3 point)
        {
            var p = new IntPoint(point.X, point.Y);
            return Clipper.PointInPolygon(p, ToClipperPath()) != 1;
        }
    }
}