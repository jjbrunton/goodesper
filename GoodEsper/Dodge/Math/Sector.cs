﻿using Buddy.Common.Math;

namespace GoodEsper.Dodge.Math
{
    public class Sector
    {
        private const int CircleLineSegmentN = 22;
        public float Angle;
        public Vector3 Center;
        public Vector3 Direction;
        public float Radius;

        public Sector(Vector3 center, Vector3 direction, float angle, float radius)
        {
            Center = center;
            Direction = direction;
            Angle = angle;
            Radius = radius;
        }

        public Polygon ToPolygon(int offset = 0)
        {
            var result = new Polygon();
            float outRadius = (Radius + offset)/(float) System.Math.Cos(2*System.Math.PI/CircleLineSegmentN);

            result.Add(Center);
            Vector3 Side1 = Direction.Rotated(-Angle*0.5f);

            for (int i = 0; i <= CircleLineSegmentN; i++)
            {
                Vector3 cDirection = Side1.Rotated(i*Angle/CircleLineSegmentN);
                cDirection.Normalize();

                result.Add(new Vector3(Center.X + outRadius*cDirection.X, Center.Y + outRadius*cDirection.Y, Center.Z));
            }

            return result;
        }
    }
}