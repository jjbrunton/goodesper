﻿using System;
using log4net;

namespace GoodEsper
{
    public static class Logger
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (GoodEsper));

        public static void Log(string message)
        {
            _log.Info(String.Format("[{0}] {1}: {2}", Info.Name, DateTime.Now.ToString("hh:mm:ss"), message));
        }
    }
}