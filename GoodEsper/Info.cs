﻿namespace GoodEsper
{
    /// <summary>
    ///     Constants class for holding routine information.
    /// </summary>
    public static class Info
    {
        public const string Author = "HeroXx";
        public const string Version = "0.0.0.1";
        public const string Name = "GoodEsper";
    }
}