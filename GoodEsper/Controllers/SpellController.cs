﻿using System;
using System.Collections.Generic;
using Buddy.Wildstar.Game;
using GoodEsper.Extensions;

namespace GoodEsper.Controllers
{
    public class SpellController
    {
        #region Fields
        private readonly List<SpellManager.SpellInformation> _stuns;
        private readonly String[] _stunList =
        {
            "Crush",
            "Incapacitate",
            "Shockwave",
            "Restraint"
        };

        private readonly List<SpellManager.SpellInformation> _psiBuilders;
        private readonly String[] _psiBuildersList =
        {
            "Telekinetic Strike",
            "Illusionary Blades",
        };

        private readonly List<SpellManager.SpellInformation> _finishers;
        private readonly String[] _finishersList =
        {
            "Telekinetic Storm",
            "Mind Burst",
        };

        private readonly List<SpellManager.SpellInformation> _generalSkills;
        private readonly String[] _generalSkillsList =
        {
            "Telekinetic Storm",
            "Mind Burst",
        };

        private readonly List<SpellManager.SpellInformation> _supportSkills;
        private readonly String[] _supportSkillsList =
        {
            "Bolster",
            "Projected Spirit",
            "Spectral Form"
        };
        #endregion

        public SpellController()
        {
            this._stuns = new List<SpellManager.SpellInformation>();
            this._finishers = new List<SpellManager.SpellInformation>();
            this._generalSkills = new List<SpellManager.SpellInformation>();
            this._psiBuilders = new List<SpellManager.SpellInformation>();
            this._supportSkills = new List<SpellManager.SpellInformation>();
            this.UpdateSpells();
        }

        private void UpdateSpells()
        {
            this._stuns.Clear();
            this._stuns.Fill(this._stunList);
            this._finishers.Clear();
            this._finishers.Fill(this._finishersList);
            this._generalSkills.Clear();
            this._generalSkills.Fill(this._generalSkillsList);
            this._psiBuilders.Clear();
            this._psiBuilders.Fill(this._psiBuildersList);
            this._supportSkills.Clear();
            this._supportSkills.Fill(this._supportSkillsList);
        }

        public SpellManager.SpellInformation GetBestOffensiveSpell()
        {
            var resource = GameManager.LocalPlayer.InnateResource;
            if (resource <= 4)
            {
                return this._psiBuilders.GetAvailableSpell();
            }

            return resource >= 5 ? this._finishers.GetAvailableSpell() : null;
        }

        public SpellManager.SpellInformation GetBestSelfSupportSpell()
        {
            var health = GameManager.LocalPlayer.HealthPercent;
            return health < 90 ? this._supportSkills.GetAvailableSpell() : null;
        }
    }
}