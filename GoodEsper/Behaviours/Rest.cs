﻿using System;
using System.Threading.Tasks;
using Buddy.Wildstar.Engine;
using Buddy.Wildstar.Game;
using GoodEsper.Controllers;

namespace GoodEsper.Behaviours
{
    public class Rest : Behaviour
    {
        private bool _resting;

        public Rest(SpellController spellController) : base(spellController)
        {
        }

        public override async Task<bool> PulseAsync()
        {
            if (!GameManager.LocalPlayer.IsInCombat && GameManager.LocalPlayer.HealthPercent < 75)
            {
                if (!_resting)
                {
                    _resting = true;
                    Logger.Log("Resting");
                }
                GameEngine.BotPulsator.Suspend(GameEngine.CurrentBot, new TimeSpan(0, 0, 1, 0));

                SpellManager.SpellInformation spell = SpellController.GetBestSelfSupportSpell();
                if (spell != null)
                {
                    spell.Cast();
                }
            }
            else if (_resting)
            {
                _resting = false;
                GameEngine.BotPulsator.Resume(GameEngine.CurrentBot);
                Logger.Log("Finished resting");
            }
            return true;
        }
    }
}