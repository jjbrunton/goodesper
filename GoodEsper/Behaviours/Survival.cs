﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Buddy.Common.Math;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using GoodEsper.Controllers;
using GoodEsper.Dodge;
using GoodEsper.Dodge.Math;
using GoodEsper.Extensions;

namespace GoodEsper.Behaviours
{
    /// <summary>
    ///     Provides a behaviour responsible for suriving where possible.
    /// </summary>
    public class Survival : Behaviour
    {
        public Survival(SpellController spellController) : base(spellController)
        {
        }

        public override async Task<bool> PulseAsync()
        {
            await Evade();
            if (Evading.IsEvading) return true;

            if (SpellController.GetBestSelfSupportSpell() != null)
            {
                await SpellController.GetBestSelfSupportSpell().Cast(GameManager.LocalPlayer);
            }

            return true;
        }

        private async Task Evade()
        {
            if (!GameManager.LocalPlayer.Position.IsInDanger())
            {
                Navigator.Stop();
                Evading.IsEvading = false;
                return;
            }

            if (!Evading.IsEvading && GameManager.LocalPlayer.Position.IsInDanger())
            {
                var points = Evading.GetSafePoints();

                if (points.Count != 0)
                {
                    var closest = GameManager.LocalPlayer.Position.Closest(points);
                    Logger.Log("Evade to: " + closest);
                    Evading.IsEvading = true;
                    await CommonBehaviors.MoveTo(closest);
                }
            }
        }
    }
}