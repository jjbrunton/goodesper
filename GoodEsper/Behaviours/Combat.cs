﻿using System;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;
using GoodEsper.Controllers;
using GoodEsper.Dodge;
using GoodEsper.Extensions;

namespace GoodEsper.Behaviours
{
    /// <summary>
    ///     Provides a combat behaviour responsible for aggression.
    /// </summary>
    public class Combat : Behaviour
    {
        private readonly Actor _me;

        public Combat(SpellController spellController) : base(spellController)
        {
            _me = GameManager.LocalPlayer;
        }

        public override async Task<bool> PulseAsync()
        {
            if (!Evading.IsEvading)
            {
                if (!GameManager.LocalPlayer.IsCasting)
                {
                    await SpellController.GetBestOffensiveSpell().Cast(GameManager.LocalPlayer.CurrentTarget);
                }
            }
            return true;
        }
    }
}