﻿using System.Threading.Tasks;
using GoodEsper.Controllers;

namespace GoodEsper.Behaviours
{
    public abstract class Behaviour
    {
        protected readonly SpellController SpellController;

        protected Behaviour(SpellController spellController)
        {
            SpellController = spellController;
        }

        public abstract Task<bool> PulseAsync();
    }
}