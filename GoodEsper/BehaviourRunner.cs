﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GoodEsper.Behaviours;

namespace GoodEsper
{
    /// <summary>
    ///     BehaviourRunner executes the class behaviour out of combat.
    /// </summary>
    public class BehaviourRunner
    {
        private readonly List<Behaviour> _combatBehaviours;

        public BehaviourRunner(List<Behaviour> behaviours)
        {
            _combatBehaviours = behaviours;
        }

        public async Task Execute()
        {
            foreach (var combatBehaviour in _combatBehaviours)
            {
                await combatBehaviour.PulseAsync();
            }
        }
    }
}