﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;

namespace GoodEsper.Extensions
{
    public static class SpellInformationExtensions
    {
        public static void Fill(this List<SpellManager.SpellInformation> target, IEnumerable<string> spells)
        {
            target.AddRange(spells.Select(SpellManager.GetSpell).Where(spell => spell != null));
        }

        public static SpellManager.SpellInformation GetAvailableSpell(this IEnumerable<SpellManager.SpellInformation> spells)
        {
            return spells.FirstOrDefault(spellInformation => !spellInformation.IsOnCooldown);
        }

        public static async Task<bool> Cast(this SpellManager.SpellInformation spell, Actor on, bool ignoreMoveInRange = false,
            bool ignoreFacing = false, int charges = 3, Func<bool> cancellation = null)
        {
            var me = GameManager.LocalPlayer;

            if (spell == null)
            {
                return false;
            }

            // Make sure we're close to the target for this ability
            // And facing it.
            // We use the client to get within range, but this is just going to be to ensure that we get a "path" to the object correct
            // May want to exclude it to be honest
            if (on != null && on != me)
            {
                if (on != GameManager.LocalPlayer.CurrentTarget)
                {
                    on.Target();
                    await Coroutine.Sleep(100);
                }

                // This is instant.
                if (!ignoreFacing)
                {
                    on.Face();
                }

                if (!ignoreMoveInRange && on.Distance > spell.TargetMaxRange)
                {
                    await
                        CommonBehaviors.MoveWithin(() => on.Position, spell.TargetMaxRange, true,
                            cancellation: cancellation);
                }
            }

            // NOTE: Almost all abilities will fail to cast if we're sprinting.
            // So make sure that we're not. Thanks!
            if (GameManager.Movement.IsSprinting)
                GameManager.Movement.Sprint(true);

            // Force the bot to stop any movement.
            if (GameManager.LocalPlayer.IsMoving)
                Navigator.Stop();

            if (spell.Name != "Bolster" && !spell.CanCast)
            {
                return false;
            }

            if (spell.Ability.IsGroundTargeted)
            {
                // If the ability is ground-targeted
                // Then we want to press the ability once to get it on the mouse
                // Then set the position of the mouse to wherever (we use the target by default)
                spell.Cast();
                GameManager.CurrentSpellPlacementPosition = on.Position;
            }

            switch (spell.SpellCastMethod)
            {
                case CastMethod.Aura:
                    spell.Cast();
                    break;

                // Cast time -> cast goes off
                case CastMethod.Normal:
                // Channeled just means that the cast happens as long as we're casting (instead of finishing at the end)
                case CastMethod.Channeled:
                case CastMethod.ChanneledField:
                // Multiphase means that there are areas of the ability that do more damage. In almost all cases, this is the "middle"
                // So ensure we're facing
                case CastMethod.Multiphase:
                    // Cast the ability, and wait for it to finish casting, or the cancellation to cause it to stop
                    spell.Cast();
                    if (await Coroutine.Wait(500, () => me.IsCasting || (cancellation != null && cancellation())))
                    {
                        await
                            Coroutine.Wait(spell.CastTime.Add(TimeSpan.FromMilliseconds(200)),
                                () => !me.IsCasting || (cancellation != null && cancellation()));
                    }
                    break;
                case CastMethod.PressHold:
                    // Button down
                    spell.Cast(true);

                    // Wait for it to start casting...
                    if (await Coroutine.Wait(500, () => me.IsCasting || (cancellation != null && cancellation())))
                    {
                        while (me.IsCasting)
                        {
                            if (!ignoreFacing)
                            {
                                on.Face();
                            }
                            await Coroutine.Yield();
                        }
                    }

                    // Wait for it to finish casting (cast time + 200ms) or the cancellation to stop it
                    await
                        Coroutine.Wait(spell.CastTime.Add(TimeSpan.FromMilliseconds(200)),
                            () => !me.IsCasting || (cancellation != null && cancellation()));

                    // Button up
                    spell.Cast(false);

                    break;
                case CastMethod.RapidTap:
                    spell.Cast();

                    // CurrentThreshold is 1 when there are charges left on rapid tap abilities
                    while (spell.CurrentThreshold != 0)
                    {
                        if (cancellation != null && cancellation())
                        {
                            return false;
                        }
                        if (!ignoreFacing)
                        {
                            on.Face();
                        }

                        spell.Cast();
                        await Coroutine.Sleep(100);
                    }

                    break;
                case CastMethod.ChargeRelease:
                    spell.Cast(true);

                    if (await Coroutine.Wait(500, () => me.IsCasting))
                    {
                        await Coroutine.Wait(10000, () => spell.CurrentThreshold >= charges);
                    }

                    spell.Cast(false);

                    break;

                // I'm not sure what these are to be completely honest...
                case CastMethod.Transactional:
                case CastMethod.Unused04:
                case CastMethod.ClientSideInteraction:
                    // I guess just cast it?
                    spell.Cast();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Logger.Log(@on != null
                ? String.Format("Casting {0} on {1}", spell.Name, @on.Name)
                : String.Format("Casting {0}", spell.Name));
            return true;
        }
    }
}
